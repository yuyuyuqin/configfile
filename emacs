(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (solarized-light)))
 '(ecb-layout-window-sizes nil)
 '(ecb-options-version "2.40")
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(tooltip-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Monaco" :foundry "unknown" :slant normal :weight normal :height 98 :width normal)))))

;;初始窗口大小
(setq initial-frame-alist '((top . 0) (left . 0) (width . 120) (height . 45)))

;; 指针颜色设置为白色
(set-cursor-color "white")

;; 鼠标颜色设置为白色
(set-mouse-color "white")

;; 语法高亮
(global-font-lock-mode t)

;; 以 y/n代表 yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; 默认显示 80列就换行
(setq default-fill-column 80)

;;显示行号
(global-linum-mode t)

;; 显示列号
(setq column-number-mode t)

;;打开图片显示功能
(auto-image-file-mode t)

;;显示括号匹配
(show-paren-mode t)

;;不产生备份文件
(setq make-backup-files nil)

;;不生成临时文件
(setq-default make-backup-files nil)

;; tab 缩进
(setq c-default-style "linux" 
	c-basic-offset 4)
(setq-default c-basic-offset 4
	indent-tabs-mode t)


;; 回车缩进
(global-set-key "\C-m" 'newline-and-indent)
(global-set-key (kbd "C-<return>") 'newline)

;;packages install
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)

;;mode-compile
(autoload 'mode-compile "mode-compile" "Command to compile current buffer file based on the major mode" t)
(global-set-key "\C-cc" 'mode-compile)
(autoload 'mode-compile-kill "mode-compile" "Command to kill a compilation launched by `mode-compile'" t)
(global-set-key "\C-ck" 'mode-compile-kill)

;;cperl-mode
(defalias 'perl-mode 'cperl-mode)
(setq cperl-highlight-variables-indiscriminately t)
(setq cperl-indent-level 4)
(setq cperl-indent-level 4
	cperl-close-paren-offset -4
	cperl-continued-statement-offset 4
	cperl-indent-parens-as-block t
	cperl-tab-always-indent t)

;;chmod
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

;;关闭出错提示
(setq visible-bell t)

;;关闭启动画面
(setq inhibit-startup-message t)
(setq gnus-inhibit-startup-message t)

;;save-place
(require 'saveplace) 
(setq-default save-place t)

;;theme
(load-theme 'monokai t)
;;(load-theme 'flatland t)


;;cedet and ecb
(setq byte-compile-warnings nil)
(require 'cedet)
(global-ede-mode 1)
(setq ecb-tip-of-the-day nil) 
(require 'ecb)
(setq stack-trace-on-error t)
(global-set-key [f7] 'ecb-activate)
(global-set-key [f8] 'ecb-deactivate)
;;窗口切换
(global-set-key [M-left] 'windmove-left)
(global-set-key [M-right] 'windmove-right)
(global-set-key [M-up] 'windmove-up)
(global-set-key [M-down] 'windmove-down)
;;最大化
(define-key global-map "\C-c1" 'ecb-maximize-window-directories)
(define-key global-map "\C-c2" 'ecb-maximize-window-sources)
(define-key global-map "\C-c3" 'ecb-maximize-window-methods)
(define-key global-map "\C-c4" 'ecb-maximize-window-history)
;;恢复原始布局
(define-key global-map "\C-c5" 'ecb-restore-default-window-sizes)

;;smart-compile
(global-set-key (kbd "<f5>") 'smart-compile)
(defun smart-compile() 
	(interactive)
	(let ((candidate-make-file-name '("makefile" "Makefile" "GNUmakefile"))
		  (command nil))
	 (if (not (null
			   (find t candidate-make-file-name :key
				'(lambda (f) (file-readable-p f)))))
	  (setq command "make -k ")
	  ;; 没有找到 Makefile ，查看当前 mode 是否是已知的可编译的模式
	  (if (null (buffer-file-name (current-buffer)))
		(message "Buffer not attached to a file, won't compile!")
		(if (eq major-mode 'c-mode)
		 (setq command
		  (concat "gcc -Wall -o "
				(file-name-sans-extension
				 (file-name-nondirectory buffer-file-name))
				" "
				(file-name-nondirectory buffer-file-name)
				" -lm "))
		 (if (eq major-mode 'c++-mode)
		  (setq command
		   (concat "g++ -std=c++11 -Wall -o "
			(file-name-sans-extension
			 (file-name-nondirectory buffer-file-name))
			" "
			(file-name-nondirectory buffer-file-name)
			" -lm"))
		  (message "Unknow mode, won't compile!")))))
	(if (not (null command))
	 (let ((command (read-from-minibuffer "Compile command: " command)))
	  (compile command)))))

;;yasnippet
;;(require 'yasnippet)
;;(yas-global-mode 1)

;;auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

;;auto-complete-clang
(require 'auto-complete-clang)

(setq ac-auto-start t)
(setq ac-quick-help-delay 0.5)
(ac-set-trigger-key "TAB")
;; (define-key ac-mode-map  [(control tab)] 'auto-complete)
(defun my-ac-config ()
  (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
  (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)
  (add-hook 'css-mode-hook 'ac-css-mode-setup)
  (add-hook 'php-mode-hook 'ac-php-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))
(defun my-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-clang) ac-sources)))
(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)
(add-hook 'c++-mode-common-hook 'my-ac-cc-mode-setup)
;; ac-source-gtags
(my-ac-config)

(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "
 /usr/include
 /usr/lib/gcc/i686-pc-linux-gnu/4.9.2/include
 /usr/lib/gcc/i686-pc-linux-gnu/4.9.2/include-fixed
 /usr/local/include
 /usr/include/c++/4.9.2
 /usr/include/c++/4.9.2/backward
 /usr/include/c++/4.9.2/i686-pc-linux-gnu
 /usr/lib/gcc/i686-pc-linux-gnu/4.9.2/include
 /usr/lib/gcc/i686-pc-linux-gnu/4.9.2/include-fixed 
 "
                )))
(global-set-key "\C-q" 'yas-exit-all-snippets)

